const { initDB } = require("./database/config");
const Student = require("./database/models/student.model");
const fs = require("fs");

async function bootstrap() {
  await initDB();
  const old = await Student.findAll({
    limit: 1,
    order: [["dateBirth", "DESC"]],
  });

  const young = await Student.findAll({
    limit: 1,
    order: [["dateBirth", "ASC"]],
  });

  fs.writeFileSync(
    "./output.json",
    JSON.stringify({
      young: young[0],
      old: old[0],
    })
  );
}

bootstrap();
